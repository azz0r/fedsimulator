export const RANKED_COLUMNS = {
  rank: { type: "rank", },
  name: { type: "string", },
  points: { type: "number", },
  wins: { type: "number", },
  losses: { type: "number", },
}

export const BATTLE_ROYAL_ENTRIES_COLUMNS = {
  rank: { type: "rank", },
  name: { type: "string", },
}

export const BATTLE_ROYAL_ELIMINATIONS_COLUMNS = {
  name: { type: "string", },
}
